<?php

declare(strict_types=1);

namespace App\Tests\Training;

use PHPUnit\Framework\TestCase;
use App\Training\TrainingManager;
use App\Training\Port\TrainingStoreInterface;
use App\Training\Exception\TrainingNotFoundException;

final class DeleteTrainingTest extends TestCase
{
    public function test_can_delete_one_training(): void
    {
        $trainingStoreMock = $this->createMock(TrainingStoreInterface::class);
        $trainingStoreMock->expects(self::once())
            ->method('hasTraining')
            ->with('dummy training')
            ->willReturn(true)
        ;
        $trainingStoreMock->expects(self::once())
            ->method('delete')
            ->with('dummy training')
        ;
        $trainingManager = new TrainingManager($trainingStoreMock);
        $trainingManager->deleteTraining('dummy training');
    }

    public function test_cannot_delete_one_unknown_training(): void
    {
        $trainingName = 'unknown training';

        $this->expectException(TrainingNotFoundException::class);
        $this->expectExceptionMessage(\sprintf('Training with name %s not found.', $trainingName));

        $trainingStoreMock = $this->createMock(TrainingStoreInterface::class);
        $trainingStoreMock->expects(self::once())
            ->method('hasTraining')
            ->with('unknown training')
        ;
        $trainingManager = new TrainingManager($trainingStoreMock);
        $trainingManager->deleteTraining($trainingName);
    }
}
