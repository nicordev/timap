<?php

declare(strict_types=1);

namespace App\Tests\Training;

use App\Tests\DataProvider\TrainingProviderTrait;
use PHPUnit\Framework\TestCase;
use App\Training\TrainingManager;
use App\Training\Port\TrainingStoreInterface;

final class ListTrainingsTest extends TestCase
{
    use TrainingProviderTrait;

    /**
     * @dataProvider provide2Trainings
     * @param array<mixed> $trainings
     */
    public function test_can_list_trainings(array $trainings): void
    {
        $trainingStoreMock = $this->createMock(TrainingStoreInterface::class);
        $trainingStoreMock->expects(self::once())
            ->method('list')
            ->with(0, 10)
            ->willReturn($trainings)
        ;
        $trainingManager = new TrainingManager($trainingStoreMock);

        $results = $trainingManager->listTrainings(offset: 0, limit: 10);

        self::assertSame($trainings, $results);
    }
}
