<?php

declare(strict_types=1);

namespace App\Tests\Training;

use PHPUnit\Framework\TestCase;
use App\Training\TrainingFactory;
use App\Training\TrainingManager;
use App\Training\Port\TrainingStoreInterface;

final class DownloadTrainingTest extends TestCase
{
    /**
     * @return array<mixed>
     */
    public static function provideTrainings(): array
    {
        return [
            [
                [
                    TrainingFactory::create([
                        'name' => 'dummy training 1',
                        'warmUp' => 3,
                        'loopsCount' => 5,
                        'rounds' => [
                            [
                                'name' => 'dummy round 1',
                                'duration' => 15,
                                'lapsCount' => 1,
                            ],
                            [
                                'name' => 'dummy round 2',
                                'duration' => 10,
                                'lapsCount' => 2,
                            ],
                        ]
                    ]),
                    TrainingFactory::create([
                        'name' => 'dummy training 2',
                        'warmUp' => 3,
                        'loopsCount' => 5,
                        'rounds' => [
                            [
                                'name' => 'dummy round 1',
                                'duration' => 15,
                                'lapsCount' => 1,
                            ],
                            [
                                'name' => 'dummy round 2',
                                'duration' => 10,
                                'lapsCount' => 2,
                            ],
                        ]
                    ]),
                ],
            ],
        ];
    }

    /**
     * @dataProvider provideTrainings
     * @param array<mixed> $trainings
     */
    public function test_can_get_training(array $trainings): void
    {
        $trainingStoreMock = $this->createMock(TrainingStoreInterface::class);
        $trainingStoreMock->expects(self::once())
            ->method('get')
            ->with('dummy training 2')
            ->willReturn($trainings[1])
        ;
        $trainingManager = new TrainingManager($trainingStoreMock);

        $results = $trainingManager->getTraining('dummy training 2');

        self::assertSame($trainings[1], $results);
    }
}
