<?php

declare(strict_types=1);

namespace App\Tests\Training;

use PHPUnit\Framework\TestCase;
use App\Training\TrainingFactory;
use App\Training\TrainingManager;
use App\Training\Port\TrainingStoreInterface;
use App\Training\Exception\TrainingCannotBeSavedException;

final class SaveTrainingTest extends TestCase
{
    public function test_can_create_a_training_manager(): void
    {
        $trainingStoreMock = $this->createMock(TrainingStoreInterface::class);
        self::assertInstanceOf(TrainingManager::class, new TrainingManager($trainingStoreMock));
    }

    public function test_can_save_one_training(): void
    {
        $expectedTraining = TrainingFactory::create([
            'name' => 'dummy training',
            'warmUp' => 3,
            'loopsCount' => 5,
            'rounds' => [
                [
                    'name' => 'dummy round 1',
                    'duration' => 15,
                    'lapsCount' => 1,
                ],
                [
                    'name' => 'dummy round 2',
                    'duration' => 10,
                    'lapsCount' => 2,
                ],
            ]
        ]);

        $trainingStoreMock = $this->createMock(TrainingStoreInterface::class);
        $trainingStoreMock->expects(self::once())
            ->method('save')
            ->with($expectedTraining)
        ;
        $trainingManager = new TrainingManager($trainingStoreMock);

        $result = $trainingManager->saveTraining([
            'name' => 'dummy training',
            'warmUp' => 3,
            'loopsCount' => 5,
            'rounds' => [
                [
                    'name' => 'dummy round 1',
                    'duration' => 15,
                    'lapsCount' => 1,
                ],
                [
                    'name' => 'dummy round 2',
                    'duration' => 10,
                    'lapsCount' => 2,
                ],
            ]
        ]);

        self::assertEquals($expectedTraining, $result);
    }

    /**
     * @return array<mixed>
     */
    public static function provideWrongTrainingData(): array
    {
        return [
            [
                [],
            ],
            [
                [
                    'warmUp' => 1,
                    'loopsCount' => 1,
                    'rounds' => [],
                ],
            ],
            [
                [
                    'name' => 'dummy training',
                    'loopsCount' => 1,
                    'rounds' => [],
                ],
            ],
            [
                [
                    'name' => 'dummy training',
                    'warmUp' => 1,
                    'rounds' => [],
                ],
            ],
            [
                [
                    'name' => 'dummy training',
                    'warmUp' => 1,
                    'loopsCount' => 1,
                ],
            ],
            [
                [
                    'name' => '',
                    'warmUp' => -1,
                    'loopsCount' => -1,
                    'rounds' => [],
                ],
            ],
            [
                [
                    'name' => null,
                    'warmUp' => '3',
                    'loopsCount' => '5',
                    'rounds' => [
                        [
                            'name' => null,
                            'duration' => 15,
                            'lapsCount' => 1,
                        ],
                        [
                            'name' => '',
                            'duration' => -15,
                            'lapsCount' => -1,
                        ],
                        [
                            'name' => 'dummy round 2',
                            'duration' => '10',
                            'lapsCount' => '2',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider provideWrongTrainingData
     * @param array<mixed> $training
     */
    public function test_cannot_save_one_training_with_wrong_data(array $training): void
    {
        $this->expectException(TrainingCannotBeSavedException::class);
        $this->expectExceptionMessage(
            "Training cannot be saved.",
        );

        $trainingStoreMock = $this->createMock(TrainingStoreInterface::class);
        $trainingStoreMock->expects(self::never())
            ->method('save')
            ->with($training)
        ;
        $trainingManager = new TrainingManager($trainingStoreMock);

        $trainingManager->saveTraining($training);
    }
}
