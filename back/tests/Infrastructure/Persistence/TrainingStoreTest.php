<?php

namespace App\Tests\Infrastructure\Persistence;

use App\Infrastructure\Persistence\Exception\TrainingEntityNotFoundException;
use App\Training\Training;
use App\Training\Round\Round;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use App\Training\Port\TrainingStoreInterface;
use App\Tests\DataProvider\TrainingProviderTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TrainingStoreTest extends KernelTestCase
{
    use TrainingProviderTrait;

    private TrainingStoreInterface $trainingStore;
    private EntityManagerInterface $entityManager;
    private Connection $database;

    private function emptyDatabase(): void
    {
        $this->database->executeStatement('delete from round;');
        $this->database->executeStatement('delete from training;');
    }

    public function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->assertSame('test', $kernel->getEnvironment());

        $this->trainingStore = static::getContainer()->get(TrainingStoreInterface::class);
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
        $this->database = $this->entityManager->getConnection();
        $this->emptyDatabase();
    }

    public function test_can_create_a_training_store(): void
    {
        self::assertInstanceOf(TrainingStoreInterface::class, $this->trainingStore);
    }

    /**
     * @dataProvider provide1Training
     */
    public function test_can_save_a_training(Training $training): void
    {
        // given a training

        // when the training store saves the training
        $this->trainingStore->save($training);

        // then training is saved
        $result = $this->database->fetchAssociative("select * from training where name = 'dummy training name'");
        self::assertIsArray($result);
    }

    /**
     * @dataProvider provide1Training
     */
    public function test_can_delete_a_training(Training $training): void
    {
        // given a training is in the database
        $this->trainingStore->save($training);

        // when the training store deletes the training
        $this->trainingStore->delete('dummy training name');

        // then training is deleted
        $result = $this->database->executeQuery("select 1 from training where name = 'dummy training name'");
        self::assertSame(0, $result->rowCount());

        // and training rounds are deleted
        $result = $this->database->executeQuery("select 1 from round r inner join training t on t.id = r.training_id and t.name = 'dummy training name'");
        self::assertSame(0, $result->rowCount());
    }

    /**
     * @dataProvider provide1Training
     */
    public function test_has_a_training(Training $training): void
    {
        // given a training is in the database
        $this->trainingStore->save($training);

        // when the training store look for this training
        $result = $this->trainingStore->hasTraining('dummy training name');

        // then the training store find the training
        self::assertTrue($result);
    }

    /**
     * @dataProvider provide1Training
     */
    public function test_has_not_a_training(Training $training): void
    {
        // given a training is in the database
        $this->trainingStore->save($training);

        // when the training store look for an unknown training
        $result = $this->trainingStore->hasTraining('unknown training name');

        // then the training store find the training
        self::assertFalse($result);
    }

    /**
     * @dataProvider provide2Trainings
     * @param array<mixed> $trainings
     */
    public function test_list_trainings(array $trainings): void
    {
        // given trainings are in the database
        \array_walk($trainings, [$this->trainingStore, 'save']);

        // when the training store list trainings
        $results = $this->trainingStore->list(0, 10);

        // then the trainings are listed
        self::assertEquals($trainings, $results);
    }

    /**
     * @dataProvider provide3Trainings
     * @param array<mixed> $trainings
     */
    public function test_list_trainings_with_offset_and_limit(array $trainings): void
    {
        // given trainings are in the database
        \array_walk($trainings, [$this->trainingStore, 'save']);

        // when the training store list trainings
        $results = $this->trainingStore->list(1, 2);

        // then the selected trainings are listed
        self::assertEquals(
            [
                $trainings[1],
                $trainings[2],
            ],
            $results
        );
    }

    /**
     * @dataProvider provide1Training
     */
    public function test_can_get_a_training(Training $training): void
    {
        // given a training is in the database
        $this->trainingStore->save($training);

        // when the training store get this training
        $result = $this->trainingStore->get('dummy training name');

        // then this training is returned
        self::assertEquals($training, $result);
    }

    /**
     * @dataProvider provide1Training
     */
    public function test_cannot_get_an_unknown_training(Training $training): void
    {
        $this->expectException(TrainingEntityNotFoundException::class);
        $this->expectExceptionMessage('Training entity with name unknown training name not found.');

        // given a training is in the database
        $this->trainingStore->save($training);

        // when the training store get an unknown training
        $this->trainingStore->get('unknown training name');

        // then an exception is thrown
    }
}
