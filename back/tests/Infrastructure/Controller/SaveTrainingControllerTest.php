<?php

namespace App\Tests\Infrastructure\Controller;

use App\Training\Training;
use PHPUnit\Framework\TestCase;
use App\Training\Port\SaveTrainingInterface;
use App\Infrastructure\Controller\Dto\RoundDto;
use App\Tests\DataProvider\TrainingProviderTrait;
use App\Infrastructure\Controller\Dto\TrainingDto;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Infrastructure\Controller\SaveTrainingController;

class SaveTrainingControllerTest extends TestCase
{
    use TrainingProviderTrait;

    /**
     * @dataProvider provide1Training
     */
    public function test_can_save_a_training(Training $training): void
    {
        $saveTraining = $this->createMock(SaveTrainingInterface::class);
        $saveTraining->expects(self::once())
            ->method('saveTraining')
            ->with([
                'name' => 'dummy training name',
                'warmUp' => 3,
                'loopsCount' => 1,
                'rounds' => [
                    [
                        'name' => 'round 1',
                        'duration' => 9,
                        'lapsCount' => 2,
                    ],
                ]
            ])
            ->willReturn($training)
        ;

        $saveTrainingController = new SaveTrainingController();
        $trainingDto = new TrainingDto(
            'dummy training name',
            3,
            1,
            [
                new RoundDto(
                    name: 'round 1',
                    duration: 9,
                    lapsCount: 2,
                ),
            ]
        );
        $response = $saveTrainingController(
            trainingDto: $trainingDto,
            saveTraining: $saveTraining,
        );

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertSame(
            json_encode([
                'name' => 'dummy training name',
                'warmUp' => 3,
                'loopsCount' => 1,
                'rounds' => [
                    [
                        'name' => 'round 1',
                        'duration' => 9,
                        'lapsCount' => 2,
                    ],
                ]
            ]),
            $response->getContent(),
        );
    }
}
