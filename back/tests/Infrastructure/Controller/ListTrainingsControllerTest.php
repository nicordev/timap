<?php

namespace App\Tests\Infrastructure\Controller;

use PHPUnit\Framework\TestCase;
use App\Tests\DataProvider\TrainingProviderTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Infrastructure\Controller\ListTrainingsController;
use App\Training\Port\ListTrainingsInterface;
use Symfony\Component\HttpFoundation\Request;

class ListTrainingsControllerTest extends TestCase
{
    use TrainingProviderTrait;

    /**
     * @dataProvider provide3Trainings
     */
    public function test_can_list_trainings(array $trainings): void
    {
        $listTrainingsController = new ListTrainingsController();

        $request = Request::create(
            uri: '/trainings',
            method: 'GET',
            parameters: [],
        );
        $listTrainings = $this->createMock(ListTrainingsInterface::class);
        $listTrainings->expects(self::once())
            ->method('listTrainings')
            ->with(0, 10)
            ->willReturn($trainings)
        ;

        $response = $listTrainingsController(
            listTrainings: $listTrainings,
            request: $request,
        );

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertJsonStringEqualsJsonString(
            '[{"name":"dummy training 1","warmUp":3,"loopsCount":5,"rounds":[{"name":"dummy round 1","duration":15,"lapsCount":1},{"name":"dummy round 2","duration":10,"lapsCount":2}]},{"name":"dummy training 2","warmUp":3,"loopsCount":5,"rounds":[{"name":"dummy round 1","duration":15,"lapsCount":1},{"name":"dummy round 2","duration":10,"lapsCount":2}]},{"name":"dummy training 3","warmUp":3,"loopsCount":5,"rounds":[{"name":"dummy round 1","duration":15,"lapsCount":1},{"name":"dummy round 2","duration":10,"lapsCount":2}]}]',
            $response->getContent(),
        );
    }

    /**
     * @dataProvider provide3Trainings
     */
    public function test_can_list_trainings_paginated(array $trainings): void
    {
        $listTrainingsController = new ListTrainingsController();

        $request = Request::create(
            uri: '/trainings?offset=4&limit=2',
            method: 'GET',
            parameters: [],
        );
        $listTrainings = $this->createMock(ListTrainingsInterface::class);
        $listTrainings->expects(self::once())
            ->method('listTrainings')
            ->with(4, 2)
            ->willReturn([])
        ;

        $listTrainingsController(
            listTrainings: $listTrainings,
            request: $request,
        );
    }
}
