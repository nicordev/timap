<?php

namespace App\Tests\Infrastructure\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SaveTrainingEndToEndTest extends WebTestCase
{
    public function test_can_save_a_training(): void
    {
        $client = static::createClient();
        $client->jsonRequest(
            'POST', 
            '/trainings',
            [
                'name' => 'dummy training name',
                'warmUp' => 1,
                'loopsCount' => 2,
                'rounds' => [
                    [
                        'name' => 'dummy round 1',
                        'duration' => 3,
                        'lapsCount' => 4,
                    ],
                ],
            ]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(201);

        $response = $client->getResponse();

        $this->assertJsonStringEqualsJsonString(
            json_encode([
                'name' => 'dummy training name',
                'warmUp' => 1,
                'loopsCount' => 2,
                'rounds' => [
                    [
                        'name' => 'dummy round 1',
                        'duration' => 3,
                        'lapsCount' => 4,
                    ],
                ]
            ]),
            $response->getContent()
        );
    }
}
