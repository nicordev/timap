<?php

namespace App\Tests\Infrastructure\Controller;

use PHPUnit\Framework\TestCase;
use App\Training\Port\DeleteTrainingInterface;
use App\Tests\DataProvider\TrainingProviderTrait;
use App\Infrastructure\Controller\DeleteTrainingController;
use Symfony\Component\HttpFoundation\Response;

class DeleteTrainingControllerTest extends TestCase
{
    use TrainingProviderTrait;

    public function test_can_delete_a_training(): void
    {
        $deleteTraining = $this->createMock(DeleteTrainingInterface::class);
        $deleteTraining->expects(self::once())
            ->method('deleteTraining')
            ->with('dummy training name')
        ;

        $deleteTrainingController = new DeleteTrainingController();
        $response = $deleteTrainingController(
            trainingName: 'dummy training name',
            deleteTraining: $deleteTraining,
        );

        self::assertInstanceOf(Response::class, $response);
        self::assertSame(204, $response->getStatusCode());
        self::assertEmpty(
            $response->getContent(),
        );
    }
}
