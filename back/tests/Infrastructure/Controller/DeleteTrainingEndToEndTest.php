<?php

namespace App\Tests\Infrastructure\Controller;

use App\Tests\DatabaseHelper\DatabaseHelperTrait;
use App\Tests\DataProvider\TrainingProviderTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

class DeleteTrainingEndToEndTest extends WebTestCase
{
    use DatabaseHelperTrait;
    use TrainingProviderTrait;

    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->setDatabase();
        $this->emptyDatabase();
    }

    /**
     * @param array<Training>
     * @dataProvider provide2Trainings
     */
    public function test_can_delete_a_training(array $trainings): void
    {
        $this->saveTrainings($trainings);

        $this->client->jsonRequest(
            'DELETE', 
            '/trainings/dummy training name',
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(204);

        $response = $this->client->getResponse();

        $this->assertEmpty($response->getContent());
    }
}
