<?php

declare(strict_types=1);

namespace App\Tests\DataProvider;

use App\Training\Training;
use App\Training\TrainingFactory;

trait TrainingProviderTrait 
{
    /**
     * @return array<string, array<Training>>
     */
    public static function provide1Training(): array
    {
        return [
            'dummy training 1' => [
                TrainingFactory::create([
                    'name' => 'dummy training name',
                    'warmUp' => 3,
                    'loopsCount' => 1,
                    'rounds' => [
                        [
                            'name' => 'round 1',
                            'duration' => 9,
                            'lapsCount' => 2,
                        ]
                    ],
                ]),
            ]
        ];
    }

    /**
     * @return array<mixed>
     */
    public static function provide2Trainings(): array
    {
        return [
            [
                [
                    TrainingFactory::create([
                        'name' => 'dummy training 1',
                        'warmUp' => 3,
                        'loopsCount' => 5,
                        'rounds' => [
                            [
                                'name' => 'dummy round 1',
                                'duration' => 15,
                                'lapsCount' => 1,
                            ],
                            [
                                'name' => 'dummy round 2',
                                'duration' => 10,
                                'lapsCount' => 2,
                            ],
                        ]
                    ]),
                    TrainingFactory::create([
                        'name' => 'dummy training 2',
                        'warmUp' => 3,
                        'loopsCount' => 5,
                        'rounds' => [
                            [
                                'name' => 'dummy round 1',
                                'duration' => 15,
                                'lapsCount' => 1,
                            ],
                            [
                                'name' => 'dummy round 2',
                                'duration' => 10,
                                'lapsCount' => 2,
                            ],
                        ]
                    ]),
                ],
            ],
        ];
    }

    /**
     * @return array<mixed>
     */
    public static function provide3Trainings(): array
    {
        return [
            [
                [
                    TrainingFactory::create([
                        'name' => 'dummy training 1',
                        'warmUp' => 3,
                        'loopsCount' => 5,
                        'rounds' => [
                            [
                                'name' => 'dummy round 1',
                                'duration' => 15,
                                'lapsCount' => 1,
                            ],
                            [
                                'name' => 'dummy round 2',
                                'duration' => 10,
                                'lapsCount' => 2,
                            ],
                        ]
                    ]),
                    TrainingFactory::create([
                        'name' => 'dummy training 2',
                        'warmUp' => 3,
                        'loopsCount' => 5,
                        'rounds' => [
                            [
                                'name' => 'dummy round 1',
                                'duration' => 15,
                                'lapsCount' => 1,
                            ],
                            [
                                'name' => 'dummy round 2',
                                'duration' => 10,
                                'lapsCount' => 2,
                            ],
                        ]
                    ]),
                    TrainingFactory::create([
                        'name' => 'dummy training 3',
                        'warmUp' => 3,
                        'loopsCount' => 5,
                        'rounds' => [
                            [
                                'name' => 'dummy round 1',
                                'duration' => 15,
                                'lapsCount' => 1,
                            ],
                            [
                                'name' => 'dummy round 2',
                                'duration' => 10,
                                'lapsCount' => 2,
                            ],
                        ]
                    ]),
                ],
            ],
        ];
    }
}
