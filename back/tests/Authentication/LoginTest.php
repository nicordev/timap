<?php

declare(strict_types=1);

namespace App\Tests\Account;

use PHPUnit\Framework\TestCase;
use App\Authentication\LoginHandler;
use App\Authentication\Port\LoginHandlerInterface;
use App\Authentication\Exception\UnknownUserException;
use App\Authentication\Exception\WrongPasswordException;
use App\Authentication\Port\AccessTokenGenerationInterface;
use App\Authentication\Port\PasswordValidationInterface;
use App\Authentication\Port\RegisteredUserStoreInterface;

final class LoginTest extends TestCase
{
    public function test_can_create_a_login_handler(): void
    {
        self::assertInstanceOf(LoginHandlerInterface::class, new LoginHandler(
            $this->createMock(RegisteredUserStoreInterface::class),
            $this->createMock(PasswordValidationInterface::class),
            $this->createMock(AccessTokenGenerationInterface::class),
        ));
    }

    public function test_can_login_registered_user_to_get_access_token(): void
    {
        $registeredUserStore = $this->createMock(RegisteredUserStoreInterface::class);
        $registeredUserStore->expects(self::once())
            ->method('isUserRegistered')
            ->with('dummy user')
            ->willReturn(true)
        ;
        $registeredUserStore->expects(self::once())
            ->method('getUserPassword')
            ->with('dummy user')
            ->willReturn('dummy password')
        ;
        $registeredUserStore->expects(self::once())
            ->method('getUserRoles')
            ->with('dummy user')
            ->willReturn([])
        ;

        $passwordValidation = $this->createMock(PasswordValidationInterface::class);
        $passwordValidation->expects(self::once())
            ->method('isPasswordValid')
            ->with('dummy password')
            ->willReturn(true)
        ;

        $accessTokenGeneration = $this->createMock(AccessTokenGenerationInterface::class);
        $accessTokenGeneration->expects(self::once())
            ->method('generateAccessToken')
            ->with(
                \json_encode([
                    'username' => 'dummy user',
                    'roles' => []
                ])
            )
            ->willReturn('dummy access token')
        ;

        $loginHandler = new LoginHandler(
            $registeredUserStore,
            $passwordValidation,
            $accessTokenGeneration,
        );
        $response = $loginHandler->loginRegisteredUser('dummy user', 'dummy password');

        self::assertSame('dummy access token', $response);
    }

    public function test_cannot_login_unregistered_user(): void
    {
        $this->expectException(UnknownUserException::class);
        $this->expectExceptionMessage('User unknown user is unknown.');

        $registeredUserStore = $this->createMock(RegisteredUserStoreInterface::class);
        $registeredUserStore->expects(self::once())
            ->method('isUserRegistered')
            ->with('unknown user')
            ->willReturn(false)
        ;

        $passwordValidation = $this->createMock(PasswordValidationInterface::class);
        $passwordValidation->expects(self::never())
            ->method('isPasswordValid')
        ;

        $accessTokenGeneration = $this->createMock(AccessTokenGenerationInterface::class);
        $accessTokenGeneration->expects(self::never())
            ->method('generateAccessToken')
        ;

        $loginHandler = new LoginHandler(
            $registeredUserStore,
            $passwordValidation,
            $accessTokenGeneration,
        );
        $loginHandler->loginRegisteredUser('unknown user', 'dummy password');
    }

    public function test_cannot_login_registered_user_with_wrong_password(): void
    {
        $this->expectException(WrongPasswordException::class);
        $this->expectExceptionMessage('Wrong password for user dummy user.');

        $registeredUserStore = $this->createMock(RegisteredUserStoreInterface::class);
        $registeredUserStore->expects(self::once())
            ->method('isUserRegistered')
            ->with('dummy user')
            ->willReturn(true)
        ;
        $registeredUserStore->expects(self::once())
            ->method('getUserPassword')
            ->with('dummy user')
            ->willReturn('dummy password')
        ;

        $passwordValidation = $this->createMock(PasswordValidationInterface::class);
        $passwordValidation->expects(self::once())
            ->method('isPasswordValid')
            ->with('wrong password')
            ->willReturn(false)
        ;

        $accessTokenGeneration = $this->createMock(AccessTokenGenerationInterface::class);
        $accessTokenGeneration->expects(self::never())
            ->method('generateAccessToken')
        ;

        $loginHandler = new LoginHandler(
            $registeredUserStore,
            $passwordValidation,
            $accessTokenGeneration,
        );
        $loginHandler->loginRegisteredUser('dummy user', 'wrong password');
    }

    public function test_can_login_registered_user_with_role_user_manager(): void
    {
        $registeredUserStore = $this->createMock(RegisteredUserStoreInterface::class);
        $registeredUserStore->expects(self::once())
            ->method('isUserRegistered')
            ->with('dummy user')
            ->willReturn(true)
        ;
        $registeredUserStore->expects(self::once())
            ->method('getUserPassword')
            ->with('dummy user')
            ->willReturn('dummy password')
        ;
        $registeredUserStore->expects(self::once())
            ->method('getUserRoles')
            ->with('dummy user')
            ->willReturn([
                'ROLE_USER_MANAGER',
            ])
        ;

        $passwordValidation = $this->createMock(PasswordValidationInterface::class);
        $passwordValidation->expects(self::once())
            ->method('isPasswordValid')
            ->with('dummy password')
            ->willReturn(true)
        ;

        $accessTokenGeneration = $this->createMock(AccessTokenGenerationInterface::class);
        $accessTokenGeneration->expects(self::once())
            ->method('generateAccessToken')
            ->with(
                \json_encode([
                    'username' => 'dummy user',
                    'roles' => [
                        'ROLE_USER_MANAGER',
                    ]
                ])
            )
            ->willReturn('dummy access token')
        ;

        $loginHandler = new LoginHandler(
            $registeredUserStore,
            $passwordValidation,
            $accessTokenGeneration,
        );
        $response = $loginHandler->loginRegisteredUser('dummy user', 'dummy password');

        self::assertSame('dummy access token', $response);
    }
}
