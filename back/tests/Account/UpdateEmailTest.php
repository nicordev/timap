<?php

declare(strict_types=1);

namespace App\Tests\Account;

use App\Account\User;
use App\Account\AccountManager;
use PHPUnit\Framework\TestCase;
use App\Account\Port\UserStoreInterface;
use App\Account\Exception\UserCannotBeRegisteredException;

final class UpdateEmailTest extends TestCase
{
    public function test_can_update_email(): void
    {
        $user = new User('dummy username', 'dummy password');

        $userStore = $this->createMock(UserStoreInterface::class);
        $userStore->expects(self::once())
            ->method('save')
            ->with($user)
            ->willReturn($user)
        ;

        $accountManager = new AccountManager($userStore);

        $result = $accountManager->registerUser([
            'username' => 'dummy username',
            'password' => 'dummy password',
        ]);

        self::assertEquals($user, $result);
    }

    /**
     * @return array<mixed>
     */
    public static function provideWrongUsersData(): array
    {
        return [
            [
                []
            ],
        ];
    }

    /**
     * @dataProvider provideWrongUsersData
     * @param array<mixed> $user
     */
    public function test_cannot_update_email_with_wrong_data(array $user): void
    {
        $this->expectException(UserCannotBeRegisteredException::class);

        $userStore = $this->createMock(UserStoreInterface::class);
        $userStore->expects(self::never())
            ->method('save')
        ;

        $accountManager = new AccountManager($userStore);

        $accountManager->registerUser($user);
    }
}
