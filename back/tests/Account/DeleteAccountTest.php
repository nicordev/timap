<?php

declare(strict_types=1);

namespace App\Tests\Account;

use App\Account\AccountManager;
use App\Account\Exception\UserCannotBeDeletedException;
use PHPUnit\Framework\TestCase;
use App\Account\Port\UserStoreInterface;
use RuntimeException;

final class DeleteAccountTest extends TestCase
{
    public function test_can_delete_an_account(): void
    {
        $userStore = $this->createMock(UserStoreInterface::class);
        $userStore->expects(self::once())
            ->method('deleteByUsername')
            ->with('dummy username')
        ;

        $accountManager = new AccountManager($userStore);

        $accountManager->deleteUser('dummy username');
    }

    public function test_cannot_delete_an_unknown_account(): void
    {
        $this->expectException(UserCannotBeDeletedException::class);

        $userStore = $this->createMock(UserStoreInterface::class);
        $userStore->expects(self::once())
            ->method('deleteByUsername')
            ->with('dummy username')
            ->willThrowException(new RuntimeException())
        ;

        $accountManager = new AccountManager($userStore);

        $accountManager->deleteUser('dummy username');
    }
}
