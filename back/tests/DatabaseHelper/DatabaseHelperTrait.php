<?php

namespace App\Tests\DatabaseHelper;

use App\Training\Training;
use App\Training\Round\Round;
use Doctrine\DBAL\Connection;

trait DatabaseHelperTrait
{
    private Connection $database;

    public function setDatabase(): void
    {
        $this->database = static::getContainer()->get('doctrine')->getManager()->getConnection();
    }

    private function emptyDatabase(): void
    {
        $this->database->executeStatement('delete from round;');
        $this->database->executeStatement('delete from training;');
    }

    /**
     * @param array<Training>
     */
    private function saveTrainings(array $trainings): void
    {
        $trainingId = 0;
        $roundId = 0;

        foreach ($trainings as $training) {
            ++$trainingId;
            $this->saveTraining($training, $trainingId);

            foreach ($training->getRounds() as $round) {
                ++$roundId;
                $this->saveRound(
                    $round,
                    $trainingId, 
                    $roundId,
                );
            }
        }
    }

    private function saveTraining(Training $training, int $trainingId): void
    {
        $sql = <<< SQL
            insert into training(id, "name", warm_up, loops_count)
            values (?, ?, ?, ?)
        SQL;
        $this->database->executeStatement($sql, [
            $trainingId,
            $training->getName(),
            $training->getWarmUp(),
            $training->getLoopsCount(),
        ]);
    }

    private function saveRound(Round $round, int $trainingId, int $roundId): void
    {
        $sql = <<< SQL
            insert into "round"(id, training_id, "name", duration, laps_count)
            values (?, ?, ?, ?, ?)
        SQL;
        $this->database->executeStatement($sql, [
            $roundId,
            $trainingId,
            $round->getName(),
            $round->getDuration(),
            $round->getLapsCount(),
        ]);
    }
}
