<?php

namespace App\Infrastructure\Controller;

use App\Training\Port\DeleteTrainingInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeleteTrainingController extends AbstractController
{
    #[Route('/trainings/{trainingName}', name: 'app_delete_training', methods: [Request::METHOD_DELETE])]
    public function __invoke(
        string $trainingName,
        DeleteTrainingInterface $deleteTraining,
    ): Response {
        $deleteTraining->deleteTraining($trainingName);

        return new Response(
            status: Response::HTTP_NO_CONTENT,
        );
    }
}
