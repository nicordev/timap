<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class TrainingDto
{
    /**
     * @param array<int, RoundDto> $rounds
     */
    public function __construct(
        #[Assert\NotBlank()]
        public readonly string $name,
        #[Assert\GreaterThanOrEqual(0)]
        public readonly int $warmUp,
        #[Assert\GreaterThan(0)]
        public readonly int $loopsCount,
        #[Assert\NotBlank()]
        /** @var <array<int, RoundDto> */
        public readonly array $rounds,
    ) {
    }

    /**
     * @return array<string, string|int|array<string, int|string>>
     */
    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'warmUp' => $this->warmUp,
            'loopsCount' => $this->loopsCount,
            'rounds' => \array_map(
                static fn (RoundDto $roundDto) => $roundDto->toArray(),
                $this->rounds,
            ),
        ];
    }
}
