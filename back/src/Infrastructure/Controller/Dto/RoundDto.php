<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class RoundDto
{
    public function __construct(
        #[Assert\NotBlank()]
        public readonly string $name,
        #[Assert\GreaterThan(0)]
        public readonly int $duration,
        #[Assert\GreaterThan(0)]
        public readonly int $lapsCount,
    ) {
    }

    /**
     * @return array<string|int>
     */
    public function toArray(): array
    {
        return \get_object_vars($this);
    }
}
