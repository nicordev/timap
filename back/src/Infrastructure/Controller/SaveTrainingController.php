<?php

namespace App\Infrastructure\Controller;

use App\Infrastructure\Controller\Dto\TrainingDto;
use App\Training\Port\SaveTrainingInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;

class SaveTrainingController extends AbstractController
{
    #[Route('/trainings', name: 'app_save_training', methods: [Request::METHOD_POST])]
    public function __invoke(
        #[MapRequestPayload] TrainingDto $trainingDto,
        SaveTrainingInterface $saveTraining,
    ): Response {
        $savedTraining = $saveTraining->saveTraining($trainingDto->toArray());

        return new JsonResponse(
            data: $savedTraining,
            status: Response::HTTP_CREATED,
        );
    }
}
