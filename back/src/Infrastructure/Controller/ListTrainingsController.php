<?php

namespace App\Infrastructure\Controller;

use App\Training\Port\ListTrainingsInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListTrainingsController extends AbstractController
{
    private const DEFAULT_LIMIT = 10;
    private const DEFAULT_OFFSET = 0;

    #[Route('/trainings', name: 'app_list_trainings', methods: [Request::METHOD_GET])]
    public function __invoke(Request $request, ListTrainingsInterface $listTrainings): Response
    {
        $offset = $request->query->get('offset') ?? self::DEFAULT_OFFSET;
        $limit = $request->query->get('limit') ?? self::DEFAULT_LIMIT;
        $trainings = $listTrainings->listTrainings($offset, $limit);

        return new JsonResponse(
            data: $trainings,
            status: Response::HTTP_CREATED,
        );
    }
}
