<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Repository;

use App\Infrastructure\Persistence\AntiCorruptionLayer\TrainingEntityConverter;
use App\Infrastructure\Persistence\Entity\Factory\TrainingEntityFactory;
use App\Infrastructure\Persistence\Entity\TrainingEntity;
use App\Infrastructure\Persistence\Exception\TrainingEntityNotFoundException;
use App\Training\Port\TrainingStoreInterface;
use App\Training\Training;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Doctrine\Persistence\ManagerRegistry;

final class TrainingRepository extends ServiceEntityRepository implements TrainingStoreInterface
{
    private readonly Connection $database;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrainingEntity::class);
        $this->database = $this->getEntityManager()->getConnection();
    }

    public function save(Training $training): Training
    {
        $trainingEntity = TrainingEntityFactory::create($training);

        $entityManager = $this->getEntityManager();

        foreach ($trainingEntity->getRounds()->toArray() as $roundEntity) {
            $entityManager->persist($roundEntity);
        }

        $entityManager->persist($trainingEntity);
        $entityManager->flush();

        return TrainingEntityConverter::convertToTraining($trainingEntity);
    }

    public function delete(string $trainingName): void
    {
        $this->deleteTrainingRounds($trainingName);
        $statement = $this->database->prepare('delete from training where name = :training_name');
        $statement->bindValue('training_name', $trainingName, ParameterType::STRING);
        $statement->executeStatement();
    }

    private function deleteTrainingRounds(string $trainingName): void
    {
        $sql = <<< SQL
            delete
            from round r
            where r.training_id = (
                select t.id
                from training t
                where t."name" = :training_name
            );
            SQL;
        $statement = $this->database->prepare($sql);
        $statement->bindValue('training_name', $trainingName, ParameterType::STRING);
        $statement->executeStatement();
    }

    public function hasTraining(string $trainingName): bool
    {
        $database = $this->getEntityManager()->getConnection();
        $statement = $database->prepare('select exists(select 1 from training where name = :training_name)');
        $statement->bindValue('training_name', $trainingName, ParameterType::STRING);
        $result = $statement->executeQuery();

        return $result->fetchOne();
    }

    /**
     * @return array<Training>
     */
    public function list(int $offset, int $limit): array
    {
        $trainingEntities = $this->createQueryBuilder('t')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;

        return \array_map([TrainingEntityConverter::class, 'convertToTraining'], $trainingEntities);
    }

    public function get(string $trainingName): Training
    {
        $trainingEntity = $this->createQueryBuilder('t')
            ->andWhere('t.name = :training_name')
            ->setParameter('training_name', $trainingName)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if (null === $trainingEntity) {
            throw new TrainingEntityNotFoundException($trainingName);
        }

        return TrainingEntityConverter::convertToTraining($trainingEntity);
    }
}
