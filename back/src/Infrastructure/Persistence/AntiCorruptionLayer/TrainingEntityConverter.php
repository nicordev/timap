<?php

namespace App\Infrastructure\Persistence\AntiCorruptionLayer;

use App\Infrastructure\Persistence\Entity\TrainingEntity;
use App\Training\Training;

final class TrainingEntityConverter
{
    public static function convertToTraining(TrainingEntity $trainingEntity): Training
    {
        return new Training(
            name: $trainingEntity->getName(),
            warmUp: $trainingEntity->getWarmUp(),
            loopsCount: $trainingEntity->getLoopsCount(),
            rounds: array_map(
                [RoundEntityConverter::class, 'convertToRound'],
                $trainingEntity->getRounds()->toArray(),
            ),
        );
    }
}
