<?php

namespace App\Infrastructure\Persistence\AntiCorruptionLayer;

use App\Infrastructure\Persistence\Entity\RoundEntity;
use App\Training\Round\Round;

final class RoundEntityConverter
{
    public static function convertToRound(RoundEntity $roundEntity): Round
    {
        return new Round(
            name: $roundEntity->getName(),
            duration: $roundEntity->getDuration(),
            lapsCount: $roundEntity->getLapsCount(),
        );
    }
}
