<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Exception;

final class TrainingEntityNotFoundException extends \RuntimeException
{
    public function __construct(string $trainingName)
    {
        parent::__construct(
            \sprintf('Training entity with name %s not found.', $trainingName)
        );
    }
}
