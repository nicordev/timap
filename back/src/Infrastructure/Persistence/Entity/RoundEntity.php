<?php

namespace App\Infrastructure\Persistence\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity()]
#[ORM\Table('round')]
final class RoundEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    private int $id;

    #[ORM\Column(type: Types::STRING)]
    private string $name;

    #[ORM\Column(type: Types::INTEGER)]
    private int $duration;

    #[ORM\Column(type: Types::INTEGER)]
    private int $lapsCount;

    #[ORM\ManyToOne(targetEntity: TrainingEntity::class, inversedBy: 'rounds')]
    #[ORM\JoinColumn(name: 'training_id', referencedColumnName: 'id', nullable: false)]
    private TrainingEntity $training;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getLapsCount(): int
    {
        return $this->lapsCount;
    }

    public function setLapsCount(int $lapsCount): self
    {
        $this->lapsCount = $lapsCount;

        return $this;
    }

    public function getTraining(): TrainingEntity
    {
        return $this->training;
    }

    public function setTraining(TrainingEntity $training): self
    {
        $this->training = $training;

        return $this;
    }
}
