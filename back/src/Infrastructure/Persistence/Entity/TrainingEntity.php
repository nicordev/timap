<?php

namespace App\Infrastructure\Persistence\Entity;

use App\Infrastructure\Persistence\Repository\TrainingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    repositoryClass: TrainingRepository::class
)]
#[ORM\Table('training')]
final class TrainingEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    private int $id;

    #[ORM\Column(type: Types::STRING)]
    private string $name;

    #[ORM\Column(type: Types::INTEGER)]
    private int $warmUp;

    #[ORM\Column(type: Types::INTEGER)]
    private int $loopsCount;

    #[ORM\OneToMany(targetEntity: RoundEntity::class, mappedBy: 'training')]
    private Collection $rounds;

    public function __construct()
    {
        $this->rounds = new ArrayCollection([]);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWarmUp(): int
    {
        return $this->warmUp;
    }

    public function setWarmUp(int $warmUp): self
    {
        $this->warmUp = $warmUp;

        return $this;
    }

    public function getLoopsCount(): int
    {
        return $this->loopsCount;
    }

    public function setLoopsCount(int $loopsCount): self
    {
        $this->loopsCount = $loopsCount;

        return $this;
    }

    /**
     * @return Collection<int, RoundEntity>
     */
    public function getRounds(): Collection
    {
        return $this->rounds;
    }

    /**
     * @param array<RoundEntity> $rounds
     */
    public function setRounds(array $rounds): self
    {
        $this->rounds = new ArrayCollection($rounds);

        return $this;
    }

    public function addRound(RoundEntity $round): void
    {
        $this->rounds->add($round);
    }
}
