<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Entity\Factory;

use App\Infrastructure\Persistence\Entity\TrainingEntity;
use App\Training\Round\Round;
use App\Training\Training;

final class TrainingEntityFactory
{
    public static function create(Training $training): TrainingEntity
    {
        $trainingEntity = (new TrainingEntity())
            ->setName($training->getName())
            ->setWarmUp($training->getWarmUp())
            ->setLoopsCount($training->getLoopsCount())
        ;
        $roundEntities = \array_map(
            static function (Round $round) use ($trainingEntity) {
                $roundEntity = RoundEntityFactory::create($round);
                $roundEntity->setTraining($trainingEntity);

                return $roundEntity;
            },
            $training->getRounds(),
        );

        $trainingEntity->setRounds($roundEntities);

        return $trainingEntity;
    }
}
