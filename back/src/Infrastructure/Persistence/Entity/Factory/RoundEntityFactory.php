<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Entity\Factory;

use App\Infrastructure\Persistence\Entity\RoundEntity;
use App\Training\Round\Round;

final class RoundEntityFactory
{
    public static function create(Round $round): RoundEntity
    {
        return (new RoundEntity())
            ->setName($round->getName())
            ->setDuration($round->getDuration())
            ->setLapsCount($round->getLapsCount())
        ;
    }
}
