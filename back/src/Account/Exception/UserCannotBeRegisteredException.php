<?php

declare(strict_types=1);

namespace App\Account\Exception;

final class UserCannotBeRegisteredException extends \RuntimeException
{
    public function __construct(\Throwable $previous = null)
    {
        parent::__construct(
            message: 'User cannot be saved.',
            previous: $previous
        );
    }
}
