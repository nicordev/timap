<?php

declare(strict_types=1);

namespace App\Account\Exception;

final class UserCannotBeDeletedException extends \RuntimeException
{
    public function __construct(string $username)
    {
        parent::__construct(
            message: \sprintf(
                'User %s cannot be deleted.',
                $username,
            ),
        );
    }
}
