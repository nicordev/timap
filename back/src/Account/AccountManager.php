<?php

declare(strict_types=1);

namespace App\Account;

use App\Account\Exception\UserCannotBeDeletedException;
use App\Account\Exception\UserCannotBeRegisteredException;
use App\Account\Port\UserStoreInterface;

final class AccountManager
{
    public function __construct(private readonly UserStoreInterface $userStore)
    {
    }

    /**
     * @param array<string, string> $userData
     */
    public function registerUser(array $userData): User
    {
        try {
            $user = new User($userData['username'], $userData['password']);

            return $this->userStore->save($user);
        } catch (\Throwable $throwable) {
            throw new UserCannotBeRegisteredException($throwable);
        }
    }

    public function deleteUser(string $username): void
    {
        try {
            $this->userStore->deleteByUsername($username);
        } catch (\Throwable $exception) {
            throw new UserCannotBeDeletedException($username);
        }
    }
}
