<?php

declare(strict_types=1);

namespace App\Account;

final class User
{
    public function __construct(
        private readonly string $username,
        private readonly string $password,
    ) {
    }
}
