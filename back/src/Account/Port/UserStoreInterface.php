<?php

declare(strict_types=1);

namespace App\Account\Port;

use App\Account\User;

interface UserStoreInterface
{
    public function save(User $user): User;

    public function deleteByUsername(string $username): void;
}
