<?php

declare(strict_types=1);

namespace App\Authentication;

use App\Authentication\Exception\UnknownUserException;
use App\Authentication\Exception\WrongPasswordException;
use App\Authentication\Port\AccessTokenGenerationInterface;
use App\Authentication\Port\LoginHandlerInterface;
use App\Authentication\Port\PasswordValidationInterface;
use App\Authentication\Port\RegisteredUserStoreInterface;

final class LoginHandler implements LoginHandlerInterface
{
    public function __construct(
        private RegisteredUserStoreInterface $registeredUserStore,
        private PasswordValidationInterface $passwordValidation,
        private AccessTokenGenerationInterface $accessTokenGeneration,
    ) {
    }

    public function loginRegisteredUser(string $username, string $password): string
    {
        if (false === $this->registeredUserStore->isUserRegistered($username)) {
            throw new UnknownUserException($username);
        }

        if (false === $this->passwordValidation->isPasswordValid($password, $this->registeredUserStore->getUserPassword($username))) {
            throw new WrongPasswordException($username);
        }

        $userRoles = $this->registeredUserStore->getUserRoles($username);

        return $this->accessTokenGeneration->generateAccessToken(
            \json_encode([
                'username' => $username,
                'roles' => $userRoles,
            ])
        );
    }
}
