<?php

declare(strict_types=1);

namespace App\Authentication\Exception;

final class WrongPasswordException extends \RuntimeException
{
    public function __construct(string $username)
    {
        parent::__construct(
            message: \sprintf(
                'Wrong password for user %s.',
                $username,
            ),
        );
    }
}
