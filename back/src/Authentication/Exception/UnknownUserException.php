<?php

declare(strict_types=1);

namespace App\Authentication\Exception;

final class UnknownUserException extends \RuntimeException
{
    public function __construct(string $username)
    {
        parent::__construct(
            message: \sprintf(
                'User %s is unknown.',
                $username,
            ),
        );
    }
}
