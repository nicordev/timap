<?php

declare(strict_types=1);

namespace App\Authentication\Port;

interface RegisteredUserStoreInterface
{
    public function isUserRegistered(string $username): bool;

    public function getUserPassword(string $username): string;

    /**
     * @return array<int, string>
     */
    public function getUserRoles(string $username): array;
}
