<?php

declare(strict_types=1);

namespace App\Authentication\Port;

interface AccessTokenGenerationInterface
{
    public function generateAccessToken(string $payload): string;
}
