<?php

declare(strict_types=1);

namespace App\Authentication\Port;

use App\Authentication\Exception\UnknownUserException;

interface LoginHandlerInterface
{
    /**
     * @throws UnknownUserException
     */
    public function loginRegisteredUser(string $username, string $password): string;
}
