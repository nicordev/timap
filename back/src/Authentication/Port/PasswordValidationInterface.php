<?php

declare(strict_types=1);

namespace App\Authentication\Port;

interface PasswordValidationInterface
{
    public function isPasswordValid(string $plainPassword, string $hashPassword): bool;
}
