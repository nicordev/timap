<?php

namespace App\Training\Round;

final class Round
{
    public function __construct(
        private readonly string $name,
        private readonly int $duration,
        private readonly int $lapsCount,
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function getLapsCount(): int
    {
        return $this->lapsCount;
    }

    /**
     * @return array<string, string|int>
     */
    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'duration' => $this->duration,
            'lapsCount' => $this->lapsCount,
        ];
    }
}
