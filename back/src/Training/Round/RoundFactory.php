<?php

declare(strict_types=1);

namespace App\Training\Round;

final class RoundFactory
{
    /**
     * @param array<string, string|int> $roundData
     */
    public static function create(
        $roundData,
    ): Round {
        return new Round(
            $roundData['name'],
            $roundData['duration'],
            $roundData['lapsCount'],
        );
    }
}
