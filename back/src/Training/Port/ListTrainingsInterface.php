<?php

declare(strict_types=1);

namespace App\Training\Port;

use App\Training\Training;

interface ListTrainingsInterface
{
    /**
     * @return array<Training>
     */
    public function listTrainings(int $offset, int $limit): array;
}
