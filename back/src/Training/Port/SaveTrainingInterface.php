<?php

declare(strict_types=1);

namespace App\Training\Port;

use App\Training\Training;

interface SaveTrainingInterface
{
    /**
     * @param array<string, string|int|array<string, string|int>> $trainingData
     */
    public function saveTraining(array $trainingData): Training;
}
