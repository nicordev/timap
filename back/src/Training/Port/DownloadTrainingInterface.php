<?php

declare(strict_types=1);

namespace App\Training\Port;

use App\Training\Training;

interface DownloadTrainingInterface
{
    public function getTraining(string $trainingName): Training;
}
