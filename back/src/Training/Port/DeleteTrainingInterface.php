<?php

declare(strict_types=1);

namespace App\Training\Port;

use App\Training\Exception\TrainingNotFoundException;

interface DeleteTrainingInterface
{
    /**
     * @throws TrainingNotFoundException
     */
    public function deleteTraining(string $trainingName): void;
}
