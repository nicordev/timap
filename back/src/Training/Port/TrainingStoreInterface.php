<?php

declare(strict_types=1);

namespace App\Training\Port;

use App\Training\Training;

interface TrainingStoreInterface
{
    public function save(Training $training): Training;

    public function delete(string $trainingName): void;

    public function hasTraining(string $trainingName): bool;

    /**
     * @return array<Training>
     */
    public function list(int $offset, int $limit): array;

    public function get(string $trainingName): Training;
}
