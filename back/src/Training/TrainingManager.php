<?php

declare(strict_types=1);

namespace App\Training;

use App\Training\Exception\TrainingCannotBeSavedException;
use App\Training\Exception\TrainingNotFoundException;
use App\Training\Port\DeleteTrainingInterface;
use App\Training\Port\DownloadTrainingInterface;
use App\Training\Port\ListTrainingsInterface;
use App\Training\Port\SaveTrainingInterface;
use App\Training\Port\TrainingStoreInterface;

final class TrainingManager implements SaveTrainingInterface, DeleteTrainingInterface, ListTrainingsInterface, DownloadTrainingInterface
{
    public function __construct(
        private readonly TrainingStoreInterface $trainingStore,
    ) {
    }

    /**
     * @param array<string, string|int|array<string, string|int>> $trainingData
     */
    public function saveTraining(array $trainingData): Training
    {
        try {
            $training = TrainingFactory::create($trainingData);
            $this->trainingStore->save($training);
        } catch (\Throwable $throwable) {
            throw new TrainingCannotBeSavedException($throwable);
        }

        return $training;
    }

    public function deleteTraining(string $trainingName): void
    {
        if (false === $this->trainingStore->hasTraining($trainingName)) {
            throw new TrainingNotFoundException($trainingName);
        }

        $this->trainingStore->delete($trainingName);
    }

    /**
     * @return array<Training>
     */
    public function listTrainings(int $offset, int $limit): array
    {
        return $this->trainingStore->list($offset, $limit);
    }

    public function getTraining(string $trainingName): Training
    {
        return $this->trainingStore->get($trainingName);
    }
}
