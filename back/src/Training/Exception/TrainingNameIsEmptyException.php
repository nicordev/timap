<?php

declare(strict_types=1);

namespace App\Training\Exception;

final class TrainingNameIsEmptyException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct(message: 'Training name is empty.');
    }
}
