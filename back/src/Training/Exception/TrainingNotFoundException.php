<?php

declare(strict_types=1);

namespace App\Training\Exception;

final class TrainingNotFoundException extends \RuntimeException
{
    public function __construct(string $trainingName)
    {
        parent::__construct(
            \sprintf('Training with name %s not found.', $trainingName)
        );
    }
}
