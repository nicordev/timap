<?php

declare(strict_types=1);

namespace App\Training\Exception;

final class TrainingHasNoRoundsException extends \RuntimeException
{
    public function __construct(string $trainingName)
    {
        parent::__construct(message: \sprintf(
            'Training [%s] has no rounds.',
            $trainingName,
        ));
    }
}
