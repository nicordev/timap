<?php

declare(strict_types=1);

namespace App\Training\Exception;

final class TrainingLoopsCountMustBeGreaterThanZeroException extends \RuntimeException
{
    public function __construct(string $trainingName)
    {
        parent::__construct(message: \sprintf(
            'Training [%s] loops count must be greater than 0.',
            $trainingName,
        ));
    }
}
