<?php

declare(strict_types=1);

namespace App\Training\Exception;

final class TrainingWarmUpMustBeGreaterThanOrEqualsToZeroException extends \RuntimeException
{
    public function __construct(string $trainingName)
    {
        parent::__construct(message: \sprintf(
            'Training [%s] warm up must be greater than or equals to 0.',
            $trainingName
        ));
    }
}
