<?php

declare(strict_types=1);

namespace App\Training\Exception;

final class TrainingCannotBeSavedException extends \RuntimeException
{
    public function __construct(\Throwable $previous = null)
    {
        parent::__construct(
            message: 'Training cannot be saved.',
            previous: $previous
        );
    }
}
