<?php

declare(strict_types=1);

namespace App\Training;

use App\Training\Round\RoundFactory;

final class TrainingFactory
{
    /**
     * @param array<string, string|int|array<string, string|int>> $trainingData
     */
    public static function create(
        array $trainingData,
    ): Training {
        $rounds = \array_map(
            [RoundFactory::class, 'create'],
            $trainingData['rounds'],
        );

        return new Training(
            $trainingData['name'],
            $trainingData['warmUp'],
            $trainingData['loopsCount'],
            $rounds,
        );
    }
}
