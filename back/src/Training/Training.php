<?php

namespace App\Training;

use App\Training\Exception\TrainingHasNoRoundsException;
use App\Training\Exception\TrainingLoopsCountMustBeGreaterThanZeroException;
use App\Training\Exception\TrainingNameIsEmptyException;
use App\Training\Exception\TrainingWarmUpMustBeGreaterThanOrEqualsToZeroException;
use App\Training\Round\Round;

// NOTE: declaring this class final will cause error: Class "App\Training\Training" is declared "final" and cannot be doubled"
class Training implements \JsonSerializable
{
    public function __construct(
        private readonly string $name,
        private readonly int $warmUp,
        private readonly int $loopsCount,
        /** @var array<Round> */
        private readonly array $rounds,
    ) {
        if ('' === $name) {
            throw new TrainingNameIsEmptyException();
        }

        if ($warmUp < 0) {
            throw new TrainingWarmUpMustBeGreaterThanOrEqualsToZeroException($name);
        }

        if ($loopsCount < 1) {
            throw new TrainingLoopsCountMustBeGreaterThanZeroException($name);
        }

        if ([] === $rounds) {
            throw new TrainingHasNoRoundsException($name);
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getWarmUp(): int
    {
        return $this->warmUp;
    }

    public function getLoopsCount(): int
    {
        return $this->loopsCount;
    }

    /**
     * @return array<Round>
     */
    public function getRounds(): array
    {
        return $this->rounds;
    }

    /**
     * @return array<string, string|int|array<string, int|string>>
     */
    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'warmUp' => $this->warmUp,
            'loopsCount' => $this->loopsCount,
            'rounds' => array_map(
                static fn (Round $round) => $round->toArray(),
                $this->rounds,
            ),
        ];
    }

    public function jsonSerialize(): mixed
    {
        return $this->toArray();
    }
}
