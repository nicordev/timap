.PHONY: dev-front
dev-front:
	cd front && quasar dev

.PHONY: test-back
test-back:
	cd back && make test
